# ![logo globaluy](logo-global.png) Normas de contribución al GKC

Ayuda para escribir en Markdown: [Markdown syntax guide](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

## Pautas generales

1. Todo tema/tecnología/procedimiento tratado debe tener asociado a él una sección el índice principal del GKC.

2. Toda sección de primer nivel en el índice principal debe ir acompañada de una imagen característica de la tecnología/tema que desarrolle y de un link a una página web (de haberla) del proyecto oficial que lo respalde, siempre que sea posible.

3. Toda sección de primer nivel del índice principal debe tener una carpeta correspondiente que encapsule los archivo *.md* que alimentan las subsecciones tratadas en ella.

4. Siempre que se pueda, cada subsección que describe un proceso o explica algo debe estar lo más aislada posible en su propio archivo *.md*, y este en la carpeta que corresponde a su sección principal.

5. No se deben crear archivos *.md* "sueltos" en la raíz del directorio.

6. No se debe usar estructura HTML en los markdowns, Bitbucket no lo soporta.

## Formato de nombres

1. Las carpetas deben comenzar con mayúscula (y respetar kebab-case en caso de más de una palabra).

2. Todos los archivos respetan la convención kebab-case.

## Imágenes

1. Los archivos de imágenes no deben ir al mismo nivel que los *.md* en cada sección (a excepción del logo de la sección).
Las opciones son crear una carpeta para contener todas las imágenes de la sección, o una carpeta por subsección conteniendo todos sus archivos.

2. Las imágenes que acompañan las secciones de primer nivel en el índice principal deben tener como ancho 150 pixeles. Y las de las subsecciones 60 pixeles.