# Múltiples environments React web

## Dos environments: Development y Production

React por defecto soporta dos environments: *development* y *production*. \
Aprovechar esto de React es bien fácil. Hay que tener en la raíz del proyecto (nivel de *src*) creados dos archivos:
* **.env.development**: Describe el environment de development/test local.
* **.env.production**: Describe el environment de producción.

![](environments-img/env-vscode.png)

Ambos archivos *.env* deberán definir las variables de entorno que la aplicación de react necesita usar.
Lo harán en la siguiente sintaxis: \
Siempre el nombre de las variables deberán llevar el prefijo **"REACT_APP_"**, luego el nombre de la variable un "=" y su valor.

Por ejemplo, si se quiere definir el valor de la variable SERVER_IP en *.env.development* con valor de *localhost:8000* se deberá escribir: \
*REACT_APP_SERVER_IP=localhost:8000*

### Usando las variables en la aplicación
Para usar el valor de las variables de entorno definidas en los archivos *.env* en la aplicación react, se busca el valor de la variable que se quiere en el objeto **process.env**.

Por ejemplo, el valor de SERVER_IP de nuestro ejemplo anterior en nuestra aplicación lo encontraremos en **process.env.REACT_APP_SERVER_IP**.

![](environments-img/ejemplo1.png)
![](environments-img/ejemplo2.png)

### Modo de uso
Una vez está bien definido lo descrito, al correr **npm start** la aplicación tomará los valores de las variables del environment de development, lo definido en *.env.development*. \
Y al correr **npm run build** la aplicación tomará los valores de las variables del environment de production para generar el build, es decir, los valores definidos en *.env.production*.

Además de eso en el primer caso existirá otra variable de entorno que node maneja y podrá ser consumida y checkeada en la aplicación, a veces es útil. \
Esta es **NODE_ENV**, y cuando se ejecuta *npm start* el valor de *NODE_ENV* es **"development"**, por otro lado cuando se ejecuta *npm run build* su valor es **"production"**.

## Más de dos environments
Esto react no lo soporta por defecto, por lo que lo más recomendable es trabajar con la ayuda de otro paquete. Un buen candidato es **env-cmd**.