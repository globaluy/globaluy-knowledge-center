# ![logo globaluy](logo-global.png) GlobalUy Knowledge Center

Aquí se documenta información relevante y guías de procesos frecuentes sobre las tecnologías más usadas en GlobalUy. \
Toda la información recopilada es brindada por el equipo de Global mediante contribuciones periódicas cada vez que 
un nuevo aprendizaje particular merece ser compartido.

[Formato de contribución al GKC (GlobalUy Knowledge Center)](contrib-guide.md)

---

# React ![react logo](React/logo.png)
[Web oficial de React](https://www.reactjs.org)

## Procesos comunes ##
* [Configurar proyecto web React con múltiples environments](React/environments.md)

---

# Django ![django logo](Django/logo.png)
[Web oficial de Django](https://www.djangoproject.com/)

## Procesos comunes

* [Deploy con Gunicorn y Nginx](Django/gunicorn-nginx.md)